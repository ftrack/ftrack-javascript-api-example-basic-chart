
*****************************************
ftrack JavaScript API: Basic Chart Widget
*****************************************

This is a basic example showing how to use the `c3js <http://c3js.org/>` for
building chart widgets in ftrack. The example shows a line chart with number of
created versions during the last 30 days.

For more information on the APIs used, please refer to the documentation:

* `Building dashboard widgets <http://ftrack.rtd.ftrack.com/en/stable/developing/building_dashboard_widgets.html>`_
* `JavaScript API client <http://ftrack-javascript-api.rtd.ftrack.com/en/stable/>`_
