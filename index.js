'use strict';
(function (ftrack, ftrackWidget) {
    var session = null;
    var days = 30;
    var depth = 5;
    var chart = null;

    /** Initialize session with credentials once widget has loaded */
    function onWidgetLoad() {
        var credentials = ftrackWidget.getCredentials();
        session = new ftrack.Session(
            credentials.serverUrl,
            credentials.apiUser,
            credentials.apiKey
        );

        console.debug('Initializing API session.');
        session.initializing.then(function () {
            console.debug('Session initialized');
        });

        var xColumn = ['x'];
        var yColumn = ['count'];

        chart = c3.generate({
            bindto: '#widget',
            data: {
                x: 'x',
                columns: [
                    xColumn,
                    yColumn
                ],
                types: {
                    count: 'area-spline'
                }
            },
            axis : {
                x : {
                    type : 'timeseries',
                    tick: {
                        culling: {
                            max: 5
                        },
                        fit: true,
                        format: "%e %b %y"
                    }
                }
            }
        });

        onWidgetUpdate();
    }

    /** Query API for name and versions when widget has loaded. */
    function onWidgetUpdate() {
        var entity = ftrackWidget.getEntity();
        console.debug('Querying new data for entity', entity);

        var start = getDateString(moment().days(-days));

        // Construct a parent filter based on a fixed depth.
        var parentQuery = [];
        for (var index = 0; index < depth; index++) {
            parentQuery.push('asset' + '.parent'.repeat(index + 1) + '.id = ' + entity.id);
        }

        var query = session.query(
            'select id, date from AssetVersion where date > "' + start + '" and (' + parentQuery.join(' or ') + ') order by date'
        );

        query.then(function (response) {
            updateInterface(response.data);
        });
    }

    /** Return a date string on the format 2116-01-01 from *date*. */
    function getDateString(date) {
        return date.toISOString().substr(0, 10);
    }

    /** Update widget UI and display *versions*. */
    function updateInterface(versions) {
        console.debug('Updating interface');
        var data = {};
        var start = moment().add(-days, 'days');
        var end = moment();

        // Generate empty dataset.
        var current = start;
        while (current <= end) {
            data[getDateString(current)] = 0;
            current = current.add(1, 'days');
        }

        // Add query result to dataset.
        for (var i = 0; i < versions.length; i++) {
            var version = versions[i];
            data[getDateString(version.date)] += 1;
        }

        // Generate c3 specific structures.
        var xColumn = ['x'];
        var yColumn = ['count'];
        for (var key in data) {
            xColumn.push(key);
            yColumn.push(data[key]);
        }

        chart.load({
            columns: [
                xColumn,
                yColumn
            ]
        });
    }

    /** Initialize widget once DOM has loaded. */
    function onDomContentLoaded() {
        console.debug('DOM content loaded, initializing widget.');
        ftrackWidget.initialize({
            onWidgetLoad: onWidgetLoad,
            onWidgetUpdate: onWidgetUpdate,
        });
    }

    window.addEventListener('DOMContentLoaded', onDomContentLoaded);
}(window.ftrack, window.ftrackWidget));